<?php

use Adianti\Control\TAction;
use Adianti\Control\TPage;
use Adianti\Registry\TSession;
use Adianti\Widget\Base\TElement;
use Adianti\Widget\Container\TPanelGroup;
use Adianti\Widget\Container\TVBox;
use Adianti\Widget\Datagrid\TDataGrid;
use Adianti\Widget\Datagrid\TDataGridAction;
use Adianti\Widget\Datagrid\TDataGridColumn;
use Adianti\Widget\Datagrid\TPageNavigation;
use Adianti\Widget\Form\TEntry;
use Adianti\Widget\Form\TLabel;
use Adianti\Widget\Form\TRadioGroup;
use Adianti\Wrapper\BootstrapDatagridWrapper;
use Adianti\Wrapper\BootstrapFormBuilder;

class AreaList extends TPage
{
    private $form;
    private $datagrid;
    private $pageNavigation;

    use Adianti\base\AdiantiStandardListTrait;

    public function __construct()
    {
        parent::__construct();
        $this->setDatabase('permission');
        $this->setActiveRecord('Area');
        $this->setDefaultOrder('descricao', 'asc');
        $this->setLimit(10);
        $this->addFilterField('descricao', 'like', 'descricao');
        $this->addFilterField('ativo', 'like', 'ativo');


        $this->form = new BootstrapFormBuilder('form_search_area');
        $this->form->setFormTitle('Área');

        $descricao  = new TEntry('descricao');
        $ativo      = new TRadioGroup('ativo');

        $descricao->setSize('100%');
        $descricao->forceUpperCase();
        $ativo->addItems( ['S' => 'Sim', 'N' => 'Não'] );
        $ativo->setLayout('horizontal');
        $ativo->setValue('S');

        $this->form->addFields([new TLabel('Descrição')], [$descricao]);
        $this->form->addFields([new TLabel('Ativo')], [$ativo]);

        $this->form->setData(TSession::getValue(__CLASS__ . '_filter_data'));

        $btn = $this->form->addAction(_t('Find'), new TAction([$this, 'onSearch']), 'fa:search');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addActionLink(_t('New'), new TAction(['AreaForm', 'onEdit'], ['register_state' => 'false']), 'fa:plus green');

        $this->datagrid = new BootstrapDatagridWrapper(new TDataGrid);
        $this->datagrid->style = 'width: 100%';

        $column_id          = new TDataGridColumn('area_id', 'Id', 'center', '10%');
        $column_descricao   = new TDataGridColumn('descricao', 'Descrição', 'center');
        $column_ativo       = new TDataGridColumn('ativo', 'Ativo', 'left');

        $column_id->setTransformer(function ($value, $object, $row) {
            if ($object->ativo == 'N') {
                $row->style = 'color: silver';
            }

            return $value;
        });

        $column_ativo->setTransformer(function ($value) {
            if ($value == 'S') {
                $div = new TElement('span');
                $div->class = "label label-success";
                $div->style = "text-shadow:none; font-size:12px";
                $div->add('Sim');
                return $div;
            } else {
                $div = new TElement('span');
                $div->class = "label label-danger";
                $div->style = "text-shadow:none; font-size:12px";
                $div->add('Não');
                return $div;
            }
        });


        $this->datagrid->addColumn($column_id);
        $this->datagrid->addColumn($column_descricao);
        $this->datagrid->addColumn($column_ativo);

        $action1 = new TDataGridAction(['AreaForm', 'onEdit'], ['id' => '{area_id}', 'register_state' => 'false']);
        $action2 = new TDataGridAction([$this, 'onDelete'], ['id' => '{area_id}']);

        $this->datagrid->addAction($action1, _t('Edit'),   'far:edit blue');
        $this->datagrid->addAction($action2, _t('Delete'), 'far:trash-alt red');

        $this->datagrid->createModel();

        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction([$this, 'onReload']));

        $panel = new TPanelGroup('', 'white');
        $panel->add($this->datagrid);
        $panel->addFooter($this->pageNavigation);

        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        $container->add($panel);

        parent::add($container);
    }
}
