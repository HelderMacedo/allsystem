<?php

use Adianti\Control\TAction;
use Adianti\Control\TPage;
use Adianti\Validator\TRequiredValidator;
use Adianti\Widget\Form\TEntry;
use Adianti\Widget\Form\TLabel;
use Adianti\Widget\Form\TRadioGroup;
use Adianti\Wrapper\BootstrapFormBuilder;

class AreaForm extends TPage
{
    private $form;

    use Adianti\Base\AdiantiStandardFormTrait;

    public function __construct()
    {
        parent::__construct();
        parent::setTargetContainer('adianti_right_panel');
        $this->setAfterSaveAction(new TAction(['AreaList', 'onReload'], ['register_state' => 'true']));
        $this->setDatabase('permission');
        $this->setActiveRecord('Area');

        $this->form = new BootstrapFormBuilder('form_area');
        $this->form->setFormTitle('Área');
        $this->form->setClientValidation(true);
        $this->form->setColumnClasses(2, ['col-sm-5 col-lg-4', 'col-sm-7 col-lg-8']);

        $id         = new TEntry('area_id');
        $descricao  = new TEntry('descricao');
        $ativo      = new TRadioGroup('ativo');

        $id->setEditable(FALSE);
        $id->setSize('100%');
        $descricao->setSize('100%');
        $descricao->addValidation('Descrição', new TRequiredValidator);
        $ativo->addItems(['S' => 'Sim', 'N' => 'Não']);

        $this->form->addFields([new TLabel('Id')], [$id]);
        $this->form->addFields([new TLabel('Descrição')], [$descricao]);
        $this->form->addFields([new TLabel('Ativo')], [$ativo]);
        $ativo->setLayout('horizontal');
        $ativo->setValue('S');

        $this->form->addHeaderActionLink(_t('Close'),  new TAction([__CLASS__, 'onClose'], ['static' => '1']), 'fa:times red');
        $btn = $this->form->addAction(_t('Save'), new TAction([$this, 'onSave']), 'fa:save');
        $btn->class = 'btn btn-sm btn-primary';

        $container = new TVBox;
        $container->style = 'width: 100%';
        // $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);

        parent::add($container);
    }

    public static function onClose($param)
    {
        TScript::create("Template.closeRightPanel()");
    }
}
