<?php

use Adianti\Control\TAction;
use Adianti\Control\TPage;
use Adianti\Registry\TSession;
use Adianti\Widget\Datagrid\TDataGrid;
use Adianti\Widget\Datagrid\TDataGridColumn;
use Adianti\Widget\Form\TEntry;
use Adianti\Widget\Form\TLabel;
use Adianti\Widget\Form\TRadioGroup;
use Adianti\Wrapper\BootstrapDatagridWrapper;
use Adianti\Wrapper\BootstrapFormBuilder;

class ExtracaoList extends TPage
{
    private $form;
    private $datagrid;
    private $pageNavigation;

    use Adianti\base\AdiantiStandardListTrait;

    public function __construct()
    {
        parent::__construct();
        $this->setDatabase('permission');
        $this->setActiveRecord('Extracao');
        $this->setDefaultOrder('descricao', 'asc');
        $this->setLimit(10);
        $this->addFilterField('descricao', 'like', 'descricao');
        $this->addFilterField('ativo', 'like', 'ativo');

        $this->form = new BootstrapFormBuilder('form_search_extracao');
        $this->form->setFormTitle('Extração');

        $descricao  = new TEntry('descricao');
        $ativo      = new TRadioGroup('ativo');

        $this->form->addFields([new TLabel('Descrição')],[$descricao]);
        $this->form->addFields([new TLabel('Ativo')], [$ativo]);

        $this->form->setData(TSession::getValue(__CLASS__ . '_filter_data'));

        $descricao->setSize('70%');
        $descricao->forceUpperCase();
        $items = ['S' => 'Sim', 'N' => 'Não'];
        $ativo->setLayout('horizontal');
        $ativo->addItems($items);
        $ativo->setUseButton();
        $ativo->setValue('S');

        $btn = $this->form->addAction(_t('Find'), new TAction([$this, 'onSearch']), 'fa:search');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addActionLink(_t('New'), new TAction(['ExtracaoForm', 'onEdit'], ['register_state' => 'false']), 'fa:plus green');

        $this->datagrid = new BootstrapDatagridWrapper(new TDataGrid);
        $this->datagrid->style = 'width: 100%';

        $column_id          = new TDataGridColumn('extracao_id', 'Extração', 'left', '10%');
        $column_descricao   = new TDataGridColumn('descricao', 'Descrição', 'left');
        $column_mobile      = new TDataGridColumn('descricao_mobile', 'Descrição Mobile', 'left');
        $column_hora        = new TDataGridColumn('hora_limite', 'Hora Limite', 'left');
        $column_premiacao   = new TDataGridColumn('premiacao_maxima', 'Premiação Máxima','left');
        $column_segunda     = new TDataGridColumn('segunda', 'Segunda', 'left');
        $column_terca       = new TDataGridColumn('terca', 'terça', 'left');
        $column_quarta      = new TDataGridColumn('quarta', 'Quarta', 'left');
        $column_quinta      = new TDataGridColumn('quinta','Quinta', 'left');
        $column_sexta       = new TDataGridColumn('sexta', 'Sexta', 'left');
        $column_sabado      = new TDataGridColumn('sabado', 'Sábado', 'left');
        $column_domingo     = new TDataGridColumn('domingo', 'domingo', 'left');
        $column_ativo       = new TDataGridColumn('ativo', 'Ativo', 'left');

        $column_id->setTransformer(function ($value, $object, $row) {
            if ($object->ativo == 'N') {
                $row->style = 'color: silver';
            }

            return $value;
        });

        $column_segunda->setTransformer(function ($value) {
            if ($value == 'S') {
                $div = new TElement('span');
                $div->class = "label label-success";
                $div->style = "text-shadow:none; font-size:12px";
                $div->add('X');
                return $div;
            } else {
                return '';
            }
        });

        $column_terca->setTransformer(function ($value) {
            if ($value == 'S') {
                $div = new TElement('span');
                $div->class = "label label-success";
                $div->style = "text-shadow:none; font-size:12px";
                $div->add('X');
                return $div;
            } else {
                return '';
            }
        });

        $column_quarta->setTransformer(function ($value) {
            if ($value == 'S') {
                $div = new TElement('span');
                $div->class = "label label-success";
                $div->style = "text-shadow:none; font-size:12px";
                $div->add('X');
                return $div;
            } else {
                return '';
            }
        });

        $column_quinta->setTransformer(function ($value) {
            if ($value == 'S') {
                $div = new TElement('span');
                $div->class = "label label-success";
                $div->style = "text-shadow:none; font-size:12px";
                $div->add('X');
                return $div;
            } else {
                return '';
            }
        });

        $column_sexta->setTransformer(function ($value) {
            if ($value == 'S') {
                $div = new TElement('span');
                $div->class = "label label-success";
                $div->style = "text-shadow:none; font-size:12px";
                $div->add('X');
                return $div;
            } else {
                return '';
            }
        });

        $column_sabado->setTransformer(function ($value) {
            if ($value == 'S') {
                $div = new TElement('span');
                $div->class = "label label-success";
                $div->style = "text-shadow:none; font-size:12px";
                $div->add('X');
                return $div;
            } else {
                return '';
            }
        });

        $column_domingo->setTransformer(function ($value) {
            if ($value == 'S') {
                $div = new TElement('span');
                $div->class = "label label-success";
                $div->style = "text-shadow:none; font-size:12px";
                $div->add('X');
                return $div;
            } else {
                return '';
            }
        });

        $column_ativo->setTransformer(function ($value) {
            if ($value == 'S') {
                $div = new TElement('span');
                $div->class = "label label-success";
                $div->style = "text-shadow:none; font-size:12px";
                $div->add('Sim');
                return $div;
            } else {
                $div = new TElement('span');
                $div->class = "label label-danger";
                $div->style = "text-shadow:none; font-size:12px";
                $div->add('Não');
                return $div;
            }
        });

        $this->datagrid->addColumn($column_descricao);
        $this->datagrid->addColumn($column_mobile);
        $this->datagrid->addColumn($column_hora);
        $this->datagrid->addColumn($column_premiacao);
        $this->datagrid->addColumn($column_segunda);
        $this->datagrid->addColumn($column_terca);
        $this->datagrid->addColumn($column_quarta);
        $this->datagrid->addColumn($column_quinta);
        $this->datagrid->addColumn($column_sexta);
        $this->datagrid->addColumn($column_sabado);
        $this->datagrid->addColumn($column_domingo);
        $this->datagrid->addColumn($column_ativo);

        $action1 = new TDataGridAction(['ExtracaoForm', 'onEdit'], ['id' => '{extracao_id}', 'register_state' => 'false']);
        $action2 = new TDataGridAction([$this, 'onDelete'], ['id' => '{extracao_id}']);

        $this->datagrid->addAction($action1, _t('Edit'),   'far:edit blue');
        $this->datagrid->addAction($action2, _t('Delete'), 'far:trash-alt red');

        $this->datagrid->createModel();

        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction([$this, 'onReload']));

        $panel = new TPanelGroup('', 'white');
        $panel->add($this->datagrid);
        $panel->addFooter($this->pageNavigation);

        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        $container->add($panel);

        parent::add($container);

    }
}