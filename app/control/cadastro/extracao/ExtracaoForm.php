<?php

use Adianti\Control\TAction;
use Adianti\Control\TPage;
use Adianti\Core\AdiantiCoreApplication;
use Adianti\Database\TCriteria;
use Adianti\Database\TFilter;
use Adianti\Database\TRepository;
use Adianti\Database\TTransaction;
use Adianti\Validator\TRequiredValidator;
use Adianti\Widget\Dialog\TMessage;
use Adianti\Widget\Form\TCheckButton;
use Adianti\Widget\Form\TCheckGroup;
use Adianti\Widget\Form\TDate;
use Adianti\Widget\Form\TEntry;
use Adianti\Widget\Form\TForm;
use Adianti\Widget\Form\TLabel;
use Adianti\Widget\Form\TRadioGroup;
use Adianti\Widget\Form\TTime;
use Adianti\Widget\Wrapper\TDBCheckGroup;
use Adianti\Widget\Wrapper\TDBCombo;
use Adianti\Widget\Wrapper\TDBSelect;
use Adianti\Wrapper\BootstrapFormBuilder;

class ExtracaoForm extends TPage
{
    private $form;

    use Adianti\Base\AdiantiStandardFormTrait;
    public function __construct()
    {
        parent::__construct();
        $this->setAfterSaveAction( new TAction(['ExtracaoList', 'onReload'], ['register_state' => 'true']) );
        $this->setDatabase('permission');
        $this->setActiveRecord('Extracao');
        $this->form = new BootstrapFormBuilder('form_extracao_form');
        $this->form->setFormTitle('Extração');
        $this->form->setClientValidation(true);

        $id                 = new TEntry('extracao_id');
        $descricao          = new TEntry('descricao');
        $mobile             = new TEntry('descricao_mobile');
        $hora               = new TTime('hora_limite');

        $criteria   = new TCriteria;
        $criteria->add(new TFilter('ativo', '=', 'S'));

        $calculo_sorteio    = new TDBCombo('calculo_id', 'permission', 'IntCalculoSorteio', 'calculo_id', 'descricao', null, $criteria);
        $premiacao_maxima   = new TEntry('premiacao_maxima');
        $data               = new TDate('dia_sorteio_inicial');
        $ultimo_sorteio     = new TEntry('ultimo_sorteio_numero');
        $segunda            = new TCheckGroup('segunda');
        $terca              = new TCheckGroup('terca');
        $quarta             = new TCheckGroup('quarta');
        $quinta             = new TCheckGroup('quinta');
        $sexta              = new TCheckGroup('sexta');
        $sabado             = new TCheckGroup('sabado');
        $domingo            = new TCheckGroup('domingo');
        $limite_palpite     = new TEntry('limite_palpite');
        $ativo              = new TRadioGroup('ativo');

        $id->setEditable(false);
        $id->setSize('100%');
        $descricao->forceUpperCase();
        $descricao->setSize('100%');
        $descricao->addValidation('Descrição', new TRequiredValidator);
        $mobile->setSize('40%');
        $mobile->forceUpperCase();
        $mobile->setMask('AAAAAA');
        $mobile->addValidation('Mobile', new TRequiredValidator);
        $hora->setSize('40%');
        $calculo_sorteio->setSize('100%');
        $calculo_sorteio->setChangeAction(new TAction([$this, 'onChangeCalculo']));
        $premiacao_maxima->setSize('40%');
        $premiacao_maxima->setMask('9!');
        $premiacao_maxima->addValidation('Premiação Máxima', new TRequiredValidator);
        $data->setSize('40%');
        $data->setDatabaseMask('');
        $data->setMask('dd/mm/yyyy');
        $data->setDatabaseMask('yyyy-mm-dd');
        $ultimo_sorteio->setSize('40%');
        $ultimo_sorteio->setMask('9!');
        //$limite_palpite->setMask('9!');
        $limite_palpite->setSize('40%');
        $limite_palpite->setNumericMask(2,',','.', true);
        $limite_palpite->style = 'text-align: right';
        $item_segunda = ['S' => 'Segunda'];
        $segunda->setUseButton();
        $segunda->addItems($item_segunda);
        $item_terca = ['S' => 'Terça'];
        $terca->addItems($item_terca);

        $item_terca = ['S' => 'Terça'];
        $terca->setUseButton();
        $terca->addItems($item_terca);

        $item_quarta = ['S' => 'Quarta'];
        $quarta->setUseButton();
        $quarta->addItems($item_quarta);

        $item_quinta = ['S' => 'Quinta'];
        $quinta->setUseButton();
        $quinta->addItems($item_quinta);

        $item_sexta = ['S' => 'Sexta'];
        $sexta->setUseButton();
        $sexta->addItems($item_sexta);

        $item_sabado = ['S' => 'Sábado'];
        $sabado->setUseButton();
        $sabado->addItems($item_sabado);

        $item_domingo = ['S' => 'Domingo'];
        $domingo->setUseButton();
        $domingo->addItems($item_domingo);

        $items_ativo = ['S' => 'Sim', 'N' => 'Não'];
        $ativo->setUseButton();
        $ativo->addItems($items_ativo);
        $ativo->setLayout('horizontal');
        $ativo->setValue('S');

        $this->form->addFields([new TLabel('Id:')], [$id]);
        $this->form->addFields([new TLabel('Descrição:')], [$descricao], [new TLabel('Descrição Mobile:')], [$mobile]);
        $this->form->addFields([new TLabel('Calculo Sorteio:')], [$calculo_sorteio], [new TLabel('Premiação Máxima:')], [$premiacao_maxima]);
        $this->form->addFields([new TLabel('Data do Primerio Sorteio:')],[$data],[new TLabel('Hora Limite:')], [$hora]);
        $this->form->addFields([new TLabel('Número do Último Sorteio:')], [$ultimo_sorteio], [new TLabel('Limite Palpite:')], [$limite_palpite]);
        $this->form->addFields([new TLabel('Dias da semana:')],[$segunda, $terca, $quarta, $quinta, $sexta, $sabado, $domingo]);
        $this->form->addFields([new TLabel('Ativo:')],[$ativo]);


        $btn = $this->form->addAction( _t('Save'), new TAction(array($this, 'onSave')), 'far:save');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addActionLink( _t('Clear'), new TAction(array($this, 'onEdit')), 'fa:eraser red');
        $this->form->addActionLink( _t('Back'), new TAction(array('ExtracaoList','onReload')), 'far:arrow-alt-circle-left blue');

        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add(new TXMLBreadCrumb('menu.xml', 'ExtracaoList'));
        $container->add($this->form);

        // add the container to the page
        parent::add($container);

    }

    public static function onChangeCalculo($param)
    {
        try{

            TTransaction::open('permission');
                $obj_calculo_sorteio = IntCalculoSorteio::find($param['calculo_id']);
                if($obj_calculo_sorteio->premiacao_maxima != 0){
                    TEntry::disableField('form_extracao_form', 'premiacao_maxima');
                }else{
                    TEntry::enableField('form_extracao_form', 'premiacao_maxima');
                }
                $obj = new stdClass;
                $obj->premiacao_maxima = $obj_calculo_sorteio->premiacao_maxima;
                TForm::sendData('form_extracao_form',$obj);
            TTransaction::close();
        }catch(Exception $e){
            new TMessage('error', $e->getMessage());
        }
    }

    public function onEdit($param)
    {
        try {
            if(isset($param['key'])){
                $key = $param['key'];
                TTransaction::open('permission');
                    $extracao = Extracao::find($key);
                    $extracao->segunda  = $extracao->segunda    == 'S' ? ['S'] : ['N'];
                    $extracao->terca    = $extracao->terca      == 'S' ? ['S'] : ['N'];
                    $extracao->quarta   = $extracao->quarta     == 'S' ? ['S'] : ['N'];
                    $extracao->quinta   = $extracao->quinta     == 'S' ? ['S'] : ['N'];
                    $extracao->sexta    = $extracao->sexta      == 'S' ? ['S'] : ['N'];
                    $extracao->sabado   = $extracao->sabado     == 'S' ? ['S'] : ['N'];
                    $extracao->domingo  = $extracao->domingo    == 'S' ? ['S'] : ['N'];

                    $obj_calculo_sorteio = IntCalculoSorteio::find($extracao->calculo_id);
                    if($obj_calculo_sorteio->premiacao_maxima != 0){
                        TEntry::disableField('form_extracao_form', 'premiacao_maxima');
                    }else{
                        TEntry::enableField('form_extracao_form', 'premiacao_maxima');
                    }
                    $this->form->setData($extracao);

                TTransaction::close();
            }else{
                $this->form->clear();
            }
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public function onSave($param)
    {
        $semanas = ['segunda','terca', 'quarta', 'quinta', 'sexta', 'sabado', 'domingo'];
        $data = $this->form->getData();
        try{
                TTransaction::open('permission');
                if(!empty($param['extracao_id'])){

                    $extracao = Extracao::find($param['extracao_id']);

                    foreach($semanas as $key => $value){
                        !empty($param[$value]) ? $extracao->$value = 'S' : $extracao->$value = 'N';
                    }
                    $extracao->descricao             = $data->descricao;
                    $extracao->descricao_mobile      = $data->descricao_mobile;
                    $extracao->hora_limite           = $data->hora_limite;
                    $extracao->premiacao_maxima      = $data->premiacao_maxima;
                    $extracao->ultimo_sorteio_numero = $data->ultimo_sorteio_numero;
                    $extracao->ativo                 = $data->ativo;
                    $extracao->dia_sorteio_inicial   = $data->dia_sorteio_inicial;
                    $extracao->limite_palpite        = $data->limite_palpite;
                    $extracao->calculo_id            = $data->calculo_id;
                    $extracao->store();

                    $obj = new stdClass;
                    $obj->extracao_id           = $extracao->extracao_id;
                    $obj->descricao             = $extracao->descricao;
                    $obj->descricao_mobile      = $extracao->descricao_mobile;
                    $obj->hora_limite           = $extracao->hora_limite;
                    $obj->premiacao_maxima      = $extracao->premiacao_maxima;
                    $obj->ultimo_sorteio_numero = $extracao->ultimo_sorteio_numero;
                    $obj->ativo                 = $extracao->ativo;
                    $obj->dia_sorteio_inicial   = $extracao->dia_sorteio_inicial;
                    $obj->limite_palpite        = $extracao->limite_palpite;
                    $obj->calculo_id            = $extracao->calculo_id;
                    $obj->segunda               = $extracao->segunda    == 'S' ? ['S'] : ['N'];
                    $obj->terca                 = $extracao->terca      == 'S' ? ['S'] : ['N'];
                    $obj->quarta                = $extracao->quarta     == 'S' ? ['S'] : ['N'];
                    $obj->quinta                = $extracao->quinta     == 'S' ? ['S'] : ['N'];
                    $obj->sexta                 = $extracao->sexta      == 'S' ? ['S'] : ['N'];
                    $obj->sabado                = $extracao->sabado     == 'S' ? ['S'] : ['N'];
                    $obj->domingo               = $extracao->domingo    == 'S' ? ['S'] : ['N'];

                    TForm::sendData('form_extracao_form', $obj);

                }else{

                    echo "Entrou";
                    $obj_extracao = new Extracao();

                    foreach($semanas as $key => $value){
                        !empty($param[$value]) ? $obj_extracao->$value = 'S' : $obj_extracao->$value = 'N';
                    }

                    $obj_extracao->descricao                = $param['descricao'];
                    $obj_extracao->descricao_mobile         = $param['descricao_mobile'];
                    $obj_extracao->hora_limite              = $param['hora_limite'];
                    $obj_extracao->premiacao_maxima         = $param['premiacao_maxima'];
                    $obj_extracao->ultimo_sorteio_numero    = $param['ultimo_sorteio_numero'];
                    $obj_extracao->ativo                    = $param['ativo'];
                    $obj_extracao->dia_sorteio_inicial      = $param['dia_sorteio_inicial'];
                    $obj_extracao->limite_palpite           = $param['limite_palpite'];
                    $obj_extracao->calculo_id               = $param['calculo_id'];
                    $obj_extracao->filtro_banca             = 1;
                    $obj_extracao->store();

                    $obj = new stdClass;
                    $obj->id = $obj_extracao->extracao_id;
                    TForm::sendData('form_extracao_form', $obj);

                }

                TTransaction::close();
                new TMessage('info', 'Registro salvo');
            }catch(Exception $e){
                new TMessage('error', $e->getMessage());
                TTransaction::rollback();
            }
    }
}