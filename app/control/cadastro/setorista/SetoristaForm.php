<?php
use Adianti\Control\TAction;
use Adianti\Control\TPage;
use Adianti\Database\TTransaction;
use Adianti\Validator\TRequiredValidator;
use Adianti\Widget\Container\TVBox;
use Adianti\Widget\Dialog\TMessage;
use Adianti\Widget\Form\TEntry;
use Adianti\Widget\Form\TForm;
use Adianti\Widget\Form\TLabel;
use Adianti\Widget\Form\TPassword;
use Adianti\Widget\Form\TRadioGroup;
use Adianti\Widget\Util\TXMLBreadCrumb;
use Adianti\Widget\Wrapper\TDBUniqueSearch;
use Adianti\Wrapper\BootstrapFormBuilder;
class SetoristaForm extends TPage
{
    private $form;
    use Adianti\Base\AdiantiStandardFormTrait;

    public function __construct()
    {
        parent::__construct();

        $this->setAfterSaveAction( new TAction(['SetoristaList', 'onReload'], ['register_state' => 'true']) );
        $this->setDatabase('permission');
        $this->setActiveRecord('Coletor');
        $this->form = new BootstrapFormBuilder('form_setorista_form');
        $this->form->setFormTitle('Setorista');
        $this->form->setClientValidation(true);

        $id             = new TEntry('coletor_id');
        $area           = new TDBUniqueSearch('area_id', 'permission', 'Area', 'area_id', 'descricao');
        $nome           = new TEntry('nome');
        $login          = new TEntry('login');
        $senha          = new TPassword('password');
        $confirma_senha = new TPassword('confirma_senha');
        $acesso_web     = new TRadioGroup('acesso_web');
        $outras_areas   = new TRadioGroup('outras_areas');
        $ativo          = new TRadioGroup('ativo');

        $id->setEditable(false);
        $id->setSize('100%');
        $area->setSize('100%');
        $area->setMinLength(0);
        $area->addValidation('Área', new TRequiredValidator);
        $nome->setSize('100%');
        $nome->addValidation('Nome', new TRequiredValidator);
        $nome->forceUpperCase();
        $login->setSize('100%');
        $login->addValidation('Login', new TRequiredValidator);
        $senha->setSize('100%');
        $confirma_senha->setSize('100%');
        $items = ['S' => 'Sim', 'N' => 'Não'];
        $acesso_web->setUseButton();
        $acesso_web->addItems($items);
        $acesso_web->setValue('S');
        $acesso_web->setLayout('horizontal');
        $acesso_web->addValidation('Acesso Web', new TRequiredValidator);
        $outras_areas->setUseButton();
        $outras_areas->addItems($items);
        $outras_areas->setLayout('horizontal');
        $outras_areas->setValue('N');
        $outras_areas->addValidation('Outras Areas', new TRequiredValidator);
        $ativo->setUseButton();
        $ativo->addItems($items);
        $ativo->setLayout('horizontal');
        $ativo->setValue('S');
        $ativo->addValidation('Ativo', new TRequiredValidator);

        $this->form->addFields([new TLabel('Id:')],[$id] ,[new TLabel('Área: ')], [$area]);
        $this->form->addFields([new TLabel('Nome:')], [$nome], [new TLabel('Login: ')], [$login]);
        $this->form->addFields([new TLabel('Senha: ')], [$senha], [new TLabel('Confirmar Senha')], [$confirma_senha]);
        $this->form->AddFields([new TLabel('Acesso Web: ')], [$acesso_web]);
        $this->form->AddFields([new TLabel('Outras Áreas: ')], [$outras_areas]);
        $this->form->AddFields([new TLabel('Ativo: ')], [$ativo]);

        $btn = $this->form->addAction( _t('Save'), new TAction(array($this, 'onSave')), 'far:save');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addActionLink( _t('Clear'), new TAction(array($this, 'onEdit')), 'fa:eraser red');
        $this->form->addActionLink( _t('Back'), new TAction(array('SetoristaList','onReload')), 'far:arrow-alt-circle-left blue');

        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add(new TXMLBreadCrumb('menu.xml', 'SetoristaList'));
        $container->add($this->form);

        // add the container to the page
        parent::add($container);

    }

    public function onSave($param)
    {
       try {
            $data = $this->form->getData();
            $this->form->setData($data);

            $object = (object) $data;

            TTransaction::open('permission');

            if(empty($object->login)){
                throw new Exception(TAdiantiCoreTranslator::translate('The field ^1 is required', _t('Login')));
            }

            if(empty($object->coletor_id)){
                if (SystemUser::newFromLogin($object->login) instanceof SystemUser)
                {
                    throw new Exception(_t('An user with this login is already registered'));
                }

                if( empty($object->password))
                {
                    throw new Exception(TAdiantiCoreTranslator::translate('The field ^1 is required', _t('Password')));
                }
            }

            if($object->password)
            {
                if($object->password !== $param['confirma_senha']){
                    throw new Exception(_t('The passwords do not match'));
                }

                $object->password = md5($object->password);
            }
            else
            {
                unset($object->password);
            }

            if(!empty($object->coletor_id)){
                $coletor    = new Coletor($object->coletor_id);
                $user       = new SystemUser($coletor->usuario_id);
                if(!empty($object->password)){
                    $user->password = $object->password;
                }
                $user->name     = $object->nome;
                $user->login = $object->login;
                $user->store();

                $coletor->nome          = $object->nome;
                $coletor->area_id       = $object->area_id;
                $coletor->acesso_web    = $object->acesso_web;
                $coletor->outras_areas  = $object->outras_areas;
                $coletor->ativo         = $object->ativo;
                $coletor->store();

            }else{
                $coletor        = new Coletor();
                $user           = new SystemUser();
                $user->login    = $object->login;
                $user->name     = $object->nome;
                $user->password = $object->password;
                $user->active   = 'S';
                $user->store();

                $coletor->nome          = $object->nome;
                $coletor->usuario_id    = $user->id;
                $coletor->area_id       = $object->area_id;
                $coletor->acesso_web    = $object->acesso_web;
                $coletor->outras_areas  = $object->outras_areas;
                $coletor->ativo         = $object->ativo;
                $coletor->store();

                $this->preencherFormAfterSaver($user, $coletor);
            }


            TTransaction::close();
            new TMessage('info', TAdiantiCoreTranslator::translate('Record saved'));
       } catch (Exception $e) {
        new TMessage('error', $e->getMessage());
        TTransaction::rollback();
       }
    }

    public function onEdit($param)
    {
        try {
            if(isset($param['key'])){
                $key = $param['key'];

                TTransaction::open('permission');
                    $object         = new Coletor($key);
                    $object->login  = $object->get_usuario()->login;

                    $this->form->setData($object);
                TTransaction::close();
            }else{
                $this->form->clear();

                TForm::sendData('form_setorista_form', ['acesso_web' => 'S', 'outras_areas' => 'N', 'ativo' => 'S']);


            }
        } catch (Exception $e) {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public function preencherFormAfterSaver($user, $coletor)
    {
        $data = [
            'coletor_id'    => $coletor->coletor_id,
            'area_id'       => $coletor->area_id,
            'nome'          => $coletor->nome,
            'login'         => $user->login,
            'acesso_web'    => $coletor->acesso_web,
            'outras_areas'  => $coletor->outras_areas,
            'ativo'         => $coletor->ativo
        ];

        TForm::sendData('form_setorista_form', $data);
    }
}