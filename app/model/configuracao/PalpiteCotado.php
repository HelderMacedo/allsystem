<?php

use Adianti\Database\TRecord;

class PalpiteCotado extends TRecord
{
    const TABLENAME     = 'cfg_palpite_cotado';
    const PRIMARYKEY    = 'palpite_cotado_id';
    const IDPOLICY      = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('palpite');
        parent::addAttribute('cotacao');
        parent::addAttribute('ativo');
        parent::addAttribute('modalidade_id');
    }

    public function get_modalidade()
    {
        return Modalidade::find($this->modalidade_id);
    }
}