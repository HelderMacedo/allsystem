<?php

use Adianti\Database\TRecord;

class AreaLimite extends TRecord
{
    const TABLENAME     = 'cfg_area_limite';
    const PRIMARYKEY    = 'area_limite_id';
    const IDPOLICY      = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('limite_palpite');
        parent::addAttribute('area_id');
        parent::addAttribute('modalidade_id');
    }

    public function get_area()
    {
        return Area::find($this->area_id);
    }

    public function get_modalidade()
    {
        return Modalidade::find($this->modalidade_id);
    }
}