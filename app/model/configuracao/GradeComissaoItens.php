<?php

use Adianti\Database\TRecord;

class GradeComissaoItens extends TRecord
{
    const TABLENAME     = 'cfg_grade_comissao_itens';
    const PRIMARYKEY    = 'grade_comissao_itens_id';
    const IDPOLICY      = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('comissao');
        parent::addAttribute('grade_comissao_id');
        parent::addAttribute('modalidade_id');
    }

    public function get_gradeComissao()
    {
        return GradeComissao::find($this->grade_comissao_id);
    }

    public function get_modalidade()
    {
        return Modalidade::find($this->modalidade_id);
    }
}