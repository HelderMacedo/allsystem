<?php

use Adianti\Database\TRecord;

class VendedorModalidadeComissao extends TRecord
{
    const TABLENAME     = 'cfg_vendedor_mod_comissao';
    const PRIMARYKEY    = 'vendedor_mod_comissao_id';
    const IDPOLICY      = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('comissao');
        parent::addAttribute('modalidade_id');
        parent::addAttribute('area_id');
        parent::addAttribute('vendedor_id');
    }

    public function get_modalidade()
    {
        return Modalidade::find($this->modalidade_id);
    }

    public function get_area()
    {
        return Area::find($this->area_id);
    }

    public function get_vendedor()
    {
        return Vendedor::find($this->vendedor_id);
    }
}