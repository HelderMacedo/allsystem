<?php

use Adianti\Database\TRecord;

class GradeComissao extends TRecord
{
    const TABLENAME     = 'cfg_grade_comissao';
    const PRIMARYKEY    = 'grade_comissao_id';
    const IDPOLICY      = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('descricao');
    }
}