<?php

use Adianti\Database\TRecord;

class ColetorArea extends TRecord
{
    const TABLENAME     = 'cfg_coletor_area';
    const PRIMARYKEY    = 'coletor_area_id';
    const IDPOLICY      = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('coletor_id');
        parent::addAttribute('area_id');
        parent::addAttribute('ativo');
        parent::addAttribute('nome');
    }

    public function get_coletor()
    {
        return Coletor::find($this->coletor_id);
    }

    public function get_area()
    {
        return Area::find($this->area_id);
    }
}