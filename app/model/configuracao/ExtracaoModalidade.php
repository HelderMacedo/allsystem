<?php

use Adianti\Database\TRecord;

class ExtracaoModalidade extends TRecord
{
    const TABLENAME     = 'cfg_extracao_modalidade';
    const PRIMARYKEY    = 'extracao_modalidade_id';
    const IDPOLICY      = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('extracao_id');
        parent::addAttribute('modalidade_id');
    }

    public function get_extracao()
    {
        return Extracao::find($this->extracao_id);
    }

    public function get_modalidade()
    {
        return Modalidade::find($this->modalidade_id);
    }
}