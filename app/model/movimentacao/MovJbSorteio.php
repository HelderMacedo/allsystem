<?php

use Adianti\Database\TRecord;

class MovJbSorteio extends TRecord
{
    const TABLENAME     = 'mov_jb_sorteio';
    const PRIMARYKEY    = 'jb_sorteio_id';
    const IDPOLICY      = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('jb_id');
        parent::addAttribute('sorteio_id');
        parent::addAttribute('modalidade_id');
        parent::addAttribute('palpites');
        parent::addAttribute('palpites_qunatidade');
        parent::addAttribute('colocacao_inicial');
        parent::addAttribute('colocacao_final');
        parent::addAttribute('valor_palpites');
        parent::addAttribute('total_sorteio');
        parent::addAttribute('comissao_sorteio');
        parent::addAttribute('sorteado');
        parent::addAttribute('sorteado_colocacao');
        parent::addAttribute('sorteado_valor');
        parent::addAttribute('sorteado_pago');
        parent::addAttribute('previsao_premio');
        parent::addAttribute('sorteado_valor');
    }

    public function get_movjb()
    {
        return MovJb::find($this->jb_id);
    }

    public function get_movsorteio()
    {
        return MovSorteio::find($this->sorteio_id);
    }

    public function get_modalidade()
    {
        return Modalidade::find($this->modalidade_id);
    }
}