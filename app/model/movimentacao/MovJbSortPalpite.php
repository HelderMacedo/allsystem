<?php

use Adianti\Database\TRecord;

class MovJbSortPalpite extends TRecord
{
    const TABLENAME     = 'mov_jb_sort_palpite';
    const PRIMARYKEY    = 'jb_palpites_id';
    const IDPOLICY      = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('jb_sorteio_id');
        parent::addAttribute('jb_id');
        parent::addAttribute('sorteio_id');
        parent::addAttribute('modalidade_id');
        parent::addAttribute('palpite');
        parent::addAttribute('valor_palpite');
        parent::addAttribute('jogou_colocacao_01');
        parent::addAttribute('jogou_colocacao_02');
        parent::addAttribute('jogou_colocacao_03');
        parent::addAttribute('jogou_colocacao_04');
        parent::addAttribute('jogou_colocacao_05');
        parent::addAttribute('jogou_colocacao_06');
        parent::addAttribute('jogou_colocacao_07');
        parent::addAttribute('jogou_colocacao_08');
        parent::addAttribute('jogou_colocacao_09');
        parent::addAttribute('jogou_colocacao_10');
        parent::addAttribute('premio_colocacao_01');
        parent::addAttribute('premio_colocacao_02');
        parent::addAttribute('premio_colocacao_03');
        parent::addAttribute('premio_colocacao_04');
        parent::addAttribute('premio_colocacao_05');
        parent::addAttribute('premio_colocacao_06');
        parent::addAttribute('premio_colocacao_07');
        parent::addAttribute('premio_colocacao_08');
        parent::addAttribute('premio_colocacao_09');
        parent::addAttribute('premio_colocacao_10');
        parent::addAttribute('ganhou_colocacao_01');
        parent::addAttribute('ganhou_colocacao_02');
        parent::addAttribute('ganhou_colocacao_03');
        parent::addAttribute('ganhou_colocacao_04');
        parent::addAttribute('ganhou_colocacao_05');
        parent::addAttribute('ganhou_colocacao_06');
        parent::addAttribute('ganhou_colocacao_07');
        parent::addAttribute('ganhou_colocacao_08');
        parent::addAttribute('ganhou_colocacao_09');
        parent::addAttribute('ganhou_colocacao_10');
        parent::addAttribute('pago_colocacao_01');
        parent::addAttribute('pago_colocacao_02');
        parent::addAttribute('pago_colocacao_03');
        parent::addAttribute('pago_colocacao_04');
        parent::addAttribute('pago_colocacao_05');
        parent::addAttribute('pago_colocacao_06');
        parent::addAttribute('pago_colocacao_07');
        parent::addAttribute('pago_colocacao_08');
        parent::addAttribute('pago_colocacao_09');
        parent::addAttribute('pago_colocacao_10');
        parent::addAttribute('pago_data_colocacao_01');
        parent::addAttribute('pago_data_colocacao_02');
        parent::addAttribute('pago_data_colocacao_03');
        parent::addAttribute('pago_data_colocacao_04');
        parent::addAttribute('pago_data_colocacao_05');
        parent::addAttribute('pago_data_colocacao_06');
        parent::addAttribute('pago_data_colocacao_07');
        parent::addAttribute('pago_data_colocacao_08');
        parent::addAttribute('pago_data_colocacao_09');
        parent::addAttribute('pago_data_colocacao_10');
        parent::addAttribute('processado_colocacao_01');
        parent::addAttribute('processado_colocacao_02');
        parent::addAttribute('processado_colocacao_03');
        parent::addAttribute('processado_colocacao_04');
        parent::addAttribute('processado_colocacao_05');
        parent::addAttribute('processado_colocacao_06');
        parent::addAttribute('processado_colocacao_07');
        parent::addAttribute('processado_colocacao_08');
        parent::addAttribute('processado_colocacao_09');
        parent::addAttribute('processado_colocacao_10');
        parent::addAttribute('pago_usuario_id');
        parent::addAttribute('ganhou_premio_total');
        parent::addAttribute('pago_premio_total');
        parent::addAttribute('pago_data_premio_total');

    }

    public function get_movjbsorteio()
    {
        return MovJbSorteio::find($this->jb_sorteio_id);
    }

    public function get_movjb()
    {
        return MovJb::find($this->jb_id);
    }

    public function get_movsorteio(){
        return MovSorteio::find($this->sorteio_id);
    }

    public function get_modalidade()
    {
        return Modalidade::find($this->modalidade_id);
    }

    public function get_user()
    {
        return SystemUser::find($this->pago_usuario_id);
    }
}