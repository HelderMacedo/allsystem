<?php

use Adianti\Database\TRecord;

class MovJb extends TRecord
{
    const TABLENAME     = 'mov_jb';
    const PRIMARYKEY    = 'jb_id';
    const IDPOLICY      = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('area_id');
        parent::addAttribute('coletor_id');
        parent::addAttribute('terminal_id');
        parent::addAttribute('sorteios_ids');
        parent::addAttribute('sorteios_quantidade');
        parent::addAttribute('vendedor_id');
        parent::addAttribute('bilhete_numero');
        parent::addAttribute('data_hora');
        parent::addAttribute('nome_cliente');
        parent::addAttribute('fone_cliente');
        parent::addAttribute('total_bilhete');
        parent::addAttribute('comissao_valor');
        parent::addAttribute('comissao_pago');
        parent::addAttribute('string_autorizacao');
        parent::addAttribute('cancelado');
        parent::addAttribute('cancelado_motivo');
        parent::addAttribute('data_reimpressao');
        parent::addAttribute('reimpressao');
        parent::addAttribute('data_cancelamento');
        parent::addAttribute('data_hora_servidor');

    }

    public function get_area()
    {
        return Area::find($this->area_id);
    }

    public function get_coletor()
    {
        return Coletor::find($this->coletor_id);
    }

    public function get_terminal()
    {
        return Terminal::find($this->terminal_id);
    }

    public function get_vendedor()
    {
        return Vendedor::find($this->vendedor_id);
    }
}