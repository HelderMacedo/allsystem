<?php

use Adianti\Database\TRecord;

class IntConfig extends TRecord
{
    const TABLENAME     = 'int_config';
    const PRIMARYKEY    = 'config_id';
    const IDPOLICY      = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('descricao');
        parent::addAttribute('nome_banca');
        parent::addAttribute('filtro_banca');
    }
}