<?php

use Adianti\Database\TRecord;

class IntGrupo extends TRecord
{
    const TABLENAME     = 'int_grupo';
    const PRIMARYKEY    = 'final_grupo_id';
    const IDPOLICY      = 'serial';

    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('grupo');
        parent::addAttribute('descricao');
        parent::addAttribute('ativo');
    }
}